# Inbox

Creates email inbox on a cpanel account
Read messages from that inbox.

You'll need to add the CPANEL access in the .env file