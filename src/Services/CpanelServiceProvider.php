<?php

namespace Eve\Inbox\Services;

use Gufy\CpanelPhp\Cpanel;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CpanelServiceProvider implements ServiceProviderInterface
{
    protected $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        /*$cpanel = new Cpanel([
            'host'        =>  getenv('CPANEL_HOST'), // ip or domain complete with its protocol and port
            'username'    =>  getenv('CPANEL_USERNAME'), // username of your server, it usually root.
            'auth_type'   =>  getenv('CPANEL_AUTH_TYPE'), // set 'hash' or 'password'
            'password'    =>  getenv('CPANEL_PASSWORD'), // long hash or your user's password
        ]);*/

        $cpanel = new Cpanel($this->settings);

        $pimple['cpanel'] = function () use ($cpanel) {
            return $cpanel;
        };
    }
}
