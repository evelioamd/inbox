<?php

namespace Eve\Inbox\Cpanel;

use Webklex\PHPIMAP\Client;
use Eve\Inbox\Transformers\FolderItemReceivedTransformer;
use Eve\Inbox\Transformers\SingleTicketTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

class ImapEmail
{
    protected $client;
    protected $settings;

    public function __construct(array $settings = [])
    {
        if (empty($settings)) {
            throw new \RuntimeException('No credentials provided');
        }

        if (!is_array($settings)) {
            throw new \RuntimeException('The provided settings must be an array');
        }

        //Connect to the IMAP Server
        $this->client = new Client($settings);

        $this->client->connect();
        $this->settings = $settings;
    }

    public function getConnection()
    {
        return $this->client->getConnection();
    }

    public function getAddress()
    {
        $address = "{" . $this->settings['host'] . ":" . $this->settings['port'] . "/" . ($this->settings['protocol'] ? $this->settings['protocol'] : 'imap');
        if (!$this->settings['validate_cert']) {
            $address .= '/novalidate-cert';
        }
        if (in_array($this->settings['encryption'], ['tls', 'notls', 'ssl'])) {
            $address .= '/' . $this->settings['encryption'];
        } elseif ($this->settings['encryption'] === "starttls") {
            $address .= '/tls';
        }
        $address .= '}';
        return $address;
    }

    public function getFolderName($folder)
    {
        switch (strtolower($folder)) {
            case 'sent':
                return 'INBOX.Sent';
            case 'drafts':
                return 'INBOX.Drafts';
            case 'inbox':
            case '':
                return 'INBOX';
            default:
                return false;
        }
    }

    public function getFolderItems($folder = '', $page = 1, $pageSize = 20)
    {
        $folder = $this->getFolderName($folder);
        if ($folder === false) {
            return false;
        }

        $inbox = $this->client->getFolder($folder);
        $total = $inbox->query()->all()->count();
        $pages = ceil($total / $page);
        if ($page > $pages) {
            $page = $pages;
        }

        $paginator = $inbox->search()
            ->get()
            ->paginate($pageSize, $page);

        $response = [
            'data' => [],
            'meta' => [
                'page' => $page,
                'total' => $total,
                'pages' => $pages,
            ],
            'success' => true,
        ];
        if ($paginator->count() > 0) {
            $items = new Collection($paginator, new FolderItemReceivedTransformer());
            $tickets = (new Manager())->createData($items)->toArray();
            $response['data'] = $tickets['data'];
        }
        return $response;
    }

    public function getMessage($uid = null, $folder = '')
    {
        $folder = $this->getFolderName($folder);
        if ($folder === false) {
            return false;
        }

        $inbox = $this->client->getFolder($folder);

        $message = $inbox->getMessage($uid);
        if ($message) {
            $message->setFlag('SEEN');
            $items = new Collection([$message], new SingleTicketTransformer());
            $tickets = (new Manager())->createData($items)->toArray();
            return $tickets;
        }

        return false;
    }
}
