<?php

namespace Eve\Inbox\Cpanel;

use Gufy\CpanelPhp\Cpanel;

class CpanelEmail
{
    protected $cpanel;
    protected $settings;

    public function __construct(array $settings = [])
    {
        if (empty($settings)) {
            throw new \RuntimeException('No credentials provided');
        }

        if (!is_array($settings)) {
            throw new \RuntimeException('The provided settings must be an array');
        }
        $this->cpanel = new Cpanel($settings);
        $this->settings = $settings;
    }


    public function checkEmailAvailability($prefix = '', $domain = '')
    {
        if ($prefix == '')
            return false;

        $email = $prefix . '@' . $domain;

        //check that the email is available at CPANEL account
        $response = $this->cpanel->execute_action(2, 'Email', 'listpopssingle', $this->settings['username'], ['regex' => $email]);
        if ($response instanceof ClientException) {
            //algo paso.
            return;
        }
        //decode the json and compare the login with the combination of $prefix + LTD_SUFFIX

        $response = json_decode($response);
        if (!empty($response->cpanelresult->data)) {
            $data = array_shift($response->cpanelresult->data);
            if ($email === $data->login) {
                return false;
            }
        }
        return true;
    }

    public function createEmailAddress($prefix = '', $domain = '', $password, $quota = 0)
    {
        if ($prefix == '')
            return false;

        //check that the email is available at CPANEL account
        $response = $this->cpanel->execute_action(2, 'Email', 'addpop', $this->settings['username'], [
            'email' => $prefix,
            'domain' => $domain,
            'password' => $password,
            'quota' => $quota
        ]);

        if ($response instanceof ClientException) {
            //algo paso.
            return;
        }
        //decode the json and compare the login with the combination of $prefix + LTD_SUFFIX
        $response = json_decode($response);
        if (!empty($response->cpanelresult->data)) {
            $data = array_shift($response->cpanelresult->data);
        }
        return true;
    }
}
