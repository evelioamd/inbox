<?php

namespace Eve\Inbox\Models;

use App\Models\Model;
use Illuminate\Support\Facades\Schema;

class MessagesQueueModel extends Model
{
    protected $table = 'eve_inbox_messages_queue';
}
