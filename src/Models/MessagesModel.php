<?php

namespace Eve\Inbox\Models;

use App\Models\Model;

class MessagesModel extends Model
{
    protected $table = 'eve_inbox_messages';
}
