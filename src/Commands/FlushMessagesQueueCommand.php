<?php

namespace Eve\Inbox\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Util\Mailer;
use Eve\Inbox\Traits\InboxTrait;
use Symfony\Component\Console\Command\LockableTrait;

class FlushMessagesQueueCommand extends Command
{
    use LockableTrait, InboxTrait;

    public function __construct(Mailer $mailer, $userClass)
    {
        $this->bootMessagesTrait($mailer, $userClass);
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:flush-messages')

            // the short description shown while running "php bin/console list"
            ->setDescription('Send all messages in queue.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will flush all mesages in the queue.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Example code
        $transport = $this->mailer->createEnqueueTransport();

        $spool = $transport->getSpool();
        //$spool->setTimeLimit(3);
        $realTransport = $this->mailer->createTransport();

        $spool->flushQueue($realTransport);

        return 0;
    }
}
