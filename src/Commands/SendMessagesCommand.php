<?php

namespace Eve\Inbox\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Eve\Inbox\Traits\InboxTrait;
use App\Models\AgentModel;
use App\Util\Mailer;
use Illuminate\Database\Query\Builder;


class SendMessagesCommand extends Command
{

    use InboxTrait;
    protected static $defaultName = 'app:send-messages';

    public function __construct(Mailer $mailer, $userClass)
    {
        $this->bootMessagesTrait($mailer, $userClass);
        parent::__construct();
    }
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            //->setName('app:send-messages')

            // the short description shown while running "php bin/console list"
            ->setDescription('Enqueue all messages.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will enqueue all mesages in the queue.')

            ->addArgument('id', InputArgument::REQUIRED, 'Message id to be enqueued');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = $input->getArgument('id');

        $output->writeLn('Sending start...');

        $counter = $this->enqueueMessage($message);

        $output->writeLn(sprintf('%d messages sent', $counter));

        return 0;
    }
}
