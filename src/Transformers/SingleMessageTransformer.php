<?php

namespace Eve\Inbox\Transformers;

use League\Fractal;

class SingleMessageTransformer extends Fractal\TransformerAbstract
{
    public function transform($item)
    {
        //$item->parseBody();
        return [
            'uid' => $item->message_id,
            'subject' => $item->subject,
            'from' => [
                'id' => $item->sender,
                'email' => $item->from,
                'name' => $item->from_name,
                'full_name' => sprintf('%s <%s>', ($item->from_name != '' ? $item->from_name : $item->from), $item->from),
            ],
            'to' => $item->to,
            'recipients' => array_map('intval', explode(',', $item->recipients)),
            //'display_to' => $this->getDisplayTo((array) $item->getTo()),
            /*'bcc' => [
                'email' => isset($item->getBcc()[0]->mail) ? $item->getBcc()[0]->mail : '',
                'name' => isset($item->getBcc()[0]->personal) ? $item->getBcc()[0]->personal : '',
                'full_name' => isset($item->getBcc()[0]->full) ? $item->getBcc()[0]->full : '',
            ],*/
            'date' => $item->updated_at->format('Y-m-d H:i:s'),
            'date_for_humans' => $item->updated_at->diffForHumans(),
            //'flags' => $item->getFlags(),
            'body' => $item->body_html
        ];
    }

    public function getDisplayTo(array $to)
    {
        $address = array_column($to, 'full');
        if (empty($address)) {
            return '';
        }
        return implode(', ', $address);
    }
}
