<?php

namespace Eve\Inbox\Transformers;

use League\Fractal\TransformerAbstract;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;

/**
 * This class convert an iterable class|array in another array ready to be encoded as json response to use as Select2 autocomplete.
 */
class RecipientsTransformer extends TransformerAbstract
{
    protected $validator;

    public function __construct()
    {
        $this->validator = new EmailValidator();
    }

    public function transform($row)
    {
        //check that the email is a valid email address syntax
        if (is_array($row)) {
            return [
                'id' => $row['id'],
                'text' => $row['id'] . ' - ' . ($row['name'] ?? $row['label'] ?? 'unset label')
            ];
        } else {
            return [
                'id' => $row->id,
                'text' => $row->id . ' - ' . ($row->name ?? $row->label ?? 'unset label')
            ];
        }
    }

    public function validateRecipient($recipient)
    {
        return $this->validator->isValid($recipient, new RFCValidation());
    }
}
