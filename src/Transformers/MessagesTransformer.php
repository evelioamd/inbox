<?php

namespace Eve\Inbox\Transformers;

use League\Fractal;

class MessagesTransformer extends Fractal\TransformerAbstract
{
    public function transform($item)
    {
        return [
            'uid' => $item->message_id,
            'subject' => $item->subject,
            'from' => [
                'email' => $item->from,
                'name' => $item->from_name,
                'full_name' => sprintf('%s <%s>', ($item->from_name != '' ? $item->from_name : $item->from), $item->from),
            ],
            'to' => $item->to,
            'date' => $item->updated_at->format('Y-m-d H:i:s'),
            'date_for_humans' => $item->updated_at->diffForHumans(),
            'folder' => $item->folder,
        ];
    }
}
