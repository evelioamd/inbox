<?php

namespace Eve\Inbox\Middleware;

use Slim\Container;
use Eve\Inbox\Services\CpanelServiceProvider;

class CpanelConnectionMiddleware
{
    protected $container;
    protected $settings;

    public function __construct(Container $container, array $settings)
    {
        $this->container = $container;
        $this->settings =  $settings;
    }

    public function __invoke($request, $response, $next)
    {
        $this->container->register(new CpanelServiceProvider($this->settings));
        return $next($request, $response);
    }
}
