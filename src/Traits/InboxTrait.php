<?php

namespace Eve\Inbox\Traits;

use League\Fractal\Manager;
use InvalidArgumentException;
use Eve\Inbox\Models\MessagesModel;
use League\Fractal\Resource\Collection;
use Egulias\EmailValidator\EmailValidator;
use Illuminate\Database\Capsule\Manager as DB;
use Eve\Inbox\Transformers\MessagesTransformer;
use Eve\Inbox\Transformers\RecipientsTransformer;
use Eve\Inbox\Transformers\SingleMessageTransformer;
use Egulias\EmailValidator\Validation\RFCValidation;
use App\Util\Mailer; //external class

trait InboxTrait
{
    protected $userClass;
    protected $validatorClass;

    protected $messageArgs = [
        'message_id',
        'folder',
        'to',
        'from',
        'subject',
        'message_headers',
        'body_text',
        'body_html',
        'recipient',
        'sender',
        'headers',
    ];

    public function bootMessagesTrait(Mailer $mailer, $userModelClass)
    {
        $this->mailer = $mailer;
        $this->userClass = $userModelClass;
        $this->validatorClass = new EmailValidator();
    }

    public function findRecipients($ids = null)
    {
        if (!is_array($ids) && $ids !== null) {
            $ids = [$ids];
        }

        $tableName = $this->userClass->getTableName();
        $query = $this->userClass->getQuery();

        $emails = $query->from($tableName . ' as t1')
            ->select(
                't1.id',
                DB::raw("CONCAT(t1.first_name, '(', t1.company_name, ')') as name"),
                't1.email',
                DB::raw("MD5(t1.id) as id_md5")
            );



        if (is_array($ids)) {
            $filteredEmail = $emails->where('status', 1)->whereIn('t1.id', $ids)->get();
        } elseif (null === $ids) {
            $filteredEmail = $emails->where('status', 1)->get();
        }

        $filteredEmail = $filteredEmail->transform(function ($item) {
            return $this->validateRecipient($item->email, new RFCValidation) ? (array) $item : null;
        });

        return $filteredEmail->filter();
    }

    public function autocompleteRecipients()
    {
        $emailRecipients = $this->findRecipients();

        $items = new Collection($emailRecipients->toArray(), new RecipientsTransformer);
        $data = (new Manager())->createData($items)->toArray();
        return $data['data'];
    }

    public function validateRecipient($recipient)
    {
        return $this->validatorClass->isValid($recipient, new RFCValidation()); //true
    }

    public function saveInboxMessage(array $messageArgs)
    {
    }

    public function enqueueMessage(string $messageId)
    {
        /*if (!isset($queueItem['message_id']) || !isset($queueItem['recipient_address'])) {
            throw new InvalidArgumentException('Required arguments missing');
        }*/
        $message = MessagesModel::where('message_id', $messageId)->first();

        if ($message && $message->id > 0) {
            //read messages attributes and create transport
            $from = [
                getenv('SMTP_FROM_EMAIL') => getenv('ADMIN_NAME')
            ];
            //in that partitucular order
            $messageMime = $this->mailer->createMessage(
                $message->subject,
                $from,
                $from,
                [],
                [],
                $message->body_html,
                'text/html'
            );

            $messageMime->getHeaders()->addTextHeader('o:tag', 'Inbox-Message');

            $transport = $this->mailer->createTransport();

            //do the enqueue fo each recipient
            $recipientsSent = $recipientsIgnored = $recipientsFailed = [];
            $recipientsOriginal = explode(',', $message->recipients);
            $recipientsSent = $message->recipients_sent != '' ? explode(',', $message->recipients_sent) : [];
            $recipients = array_diff($recipientsOriginal, $recipientsSent);
            $totalSent = 0;
            foreach ($recipients as $recipient) {
                $recipientUser  = $this->findRecipients($recipient);
                if ($recipientUser) {
                    $recipientUser = $recipientUser->first();
                    $messageMime->setTo([
                        $recipientUser['email'] => $recipientUser['name']
                    ]);
                    $sent = $this->mailer->sendEmail($transport, $messageMime);
                    if ($sent > 0) {
                        //sent email
                        $recipientsSent[] = $recipient;
                        $totalSent += 1;
                    } else {
                        //failder email
                        $recipientsFailed[] = $recipient;
                    }
                } else {
                    //ignore email
                    $recipientsIgnored[] = $recipient;
                }
            }
            sort($recipientsSent);
            sort($recipientsFailed);
            sort($recipientsIgnored);
            $message->recipients_sent = implode(',', $recipientsSent);
            $message->recipients_failed = implode(',', $recipientsFailed);
            $message->recipients_ignored = implode(',', $recipientsIgnored);

            $message->save();

            return $totalSent;
        }
        return false;
    }

    public function getMessages($folder = 'inbox', $recipient = null, $sort = 'desc')
    {
        $messages = 'null';
        if (is_int($recipient) && $folder == 'inbox') {
            //inbox of an affiliate
            $messages = MessagesModel::whereRaw('FIND_IN_SET(?, `recipients`)', [$recipient]);
        } elseif (is_int($recipient) && (strtoupper($folder) == 'SENT' || strtoupper($folder) == 'DRAFTS')) {
            //sent and drafts by user
            $recipient =  $this->findRecipients($recipient)->first();
            $messages = MessagesModel::where('from', $recipient['email'])->where('folder', $folder);
        } elseif (null === $recipient && (strtoupper($folder) == 'DRAFTS' || strtoupper($folder) == 'SENT')) {
            //sent and drafts by admin
            $messages = MessagesModel::where([['recipients', '<>', ''], ['folder', strtolower($folder)], ['recipients', '<>', 0]]);
        } elseif (null === $recipient) {
            //inbox for admins
            $messages = MessagesModel::where('recipients', 0);
        }

        if ($messages) {
            $messages = $messages->orderBy('updated_at', $sort)->get();
            $items = new Collection($messages, new MessagesTransformer());
            $tickets = (new Manager())->createData($items)->toArray();
            return $tickets['data'];
        }
    }

    public function getMessage($id, $folder = '')
    {
        $messages = MessagesModel::where('message_id', $id)->first();
        $items = new Collection([$messages], new SingleMessageTransformer());
        return (new Manager())->createData($items)->toArray();
    }

    public function createMessageFromAdmin($data)
    {
        //$data['sender'] = 0;
        $recipientsIds = $data['recipients'];
        $subject = $data['subject'];
        $body = $data['body'];
        $asDraft = (bool) $data['as_draft'] ?? 0;

        if (!is_array($recipientsIds)) {
            $recipientsIds = [$recipientsIds];
        }

        $recipients = $this->findRecipients($recipientsIds);
        $findedAddresses = $recipients->toArray();

        //swinfmailer formar TO
        $toAddresses = array_column($findedAddresses, 'name', 'email');

        //SEND NOTIFICATIONS
        $from = [
            getenv('SMTP_FROM_EMAIL') => getenv('ADMIN_NAME')
        ];
        //in that partitucular order
        $mailerArgs = [
            $subject,
            $from,
            $toAddresses,
            [],
            [],
            $body,
            'text/html'
        ];

        if ($asDraft == 1) {
            $folder = 'drafts';
        } else {
            $folder = 'sent';
        }

        //notify the admin always
        if (isset($data['uid']) && $data['uid'] != '') {
            $uid = $data['uid'];
        } else {
            $messageMime = $this->mailer->createMessage(...$mailerArgs);
            $splitedId = explode('@', $messageMime->getId());
            $uid = reset($splitedId);
        }
        $inReplyTo = isset($data['in_reply_to']) ? $data['in_reply_to'] : '';


        $messageData = [
            'message_id' => $uid,
            'in_reply_to' => $inReplyTo,
            'folder' => $folder,
            'to' => implode(',', $toAddresses),
            'from' => getenv('SMTP_FROM_EMAIL'),
            'from_name' => getenv('APP_NAME'),
            'subject' => $subject,
            'body_html' => $body,
            'recipients' => implode(',', $recipientsIds),
            'total_recipients' => $recipients->count(),
            'sender' => 0
        ];

        return $messageData;
    }


    public function sendMessageFromAdmin($post = [])
    {
        if (empty($post)) {
            return [
                'success' => false,
                'message' => 'Unable to compose message, missing information.'
            ];
        }

        $messageData = $this->createMessageFromAdmin($post);

        if (isset($post['uid']) && $post['uid'] != '') {
            $message = MessagesModel::where('message_id', $post['uid'])->first();
        } else {
            $message = new MessagesModel;
        }

        foreach ($messageData as $k => $v) {
            $message->{$k} = $v;
        }

        $message->save();

        if (strtoupper($message->folder) == 'DRAFTS') {
            $resp = [
                'success' => true,
                'message' => 'The message has been save to drafts.'
            ];
        } elseif (strtoupper($message->folder) == 'SENT') {
            //queue messages to be send individually(command)
            $command = sprintf('php-cli %sbin/cli.php app:send-messages %s', ROOT,  $message->message_id);
            shell_exec("nohup " . $command . " > /dev/null &");
            $resp = [
                'success' => true,
                'message' => sprintf('The message has been enqueued to be sent to %s contacts.', $message->total_recipients)
            ];
        }

        return $resp;
    }

    public function createMessageFromAgent($senderId = 0, $data)
    {
        $subject = $data['subject'];
        $body = $data['body'];

        if (is_int($senderId)) {
            $senderId = [$senderId];
        }

        $senderId = $this->findRecipients($senderId);
        $sender = $senderId->first();

        $data['sender'] = $sender['id'];

        //swinfmailer formar TO
        $from = [
            $sender['email'] => $sender['name']
        ];

        //SEND NOTIFICATIONS
        $to = [
            getenv('ADMIN_EMAIL') => getenv('APP_NAME')
        ];
        //in that partitucular order
        $mailerArgs = [
            $subject,
            $from,
            $to,
            [],
            [],
            $body,
            'text/html'
        ];

        //notify the admin always
        if (isset($data['uid']) && $data['uid'] != '') {
            $uid = $data['uid'];
        } else {
            $messageMime = $this->mailer->createMessage(...$mailerArgs);
            $splitedId = explode('@', $messageMime->getId());
            $uid = reset($splitedId);
        }

        $inReplyTo = isset($data['in_reply_to']) ? $data['in_reply_to'] : '';
        $transport = $this->mailer->createTransport();
        $this->mailer->sendEmail($transport, $messageMime); //try to send to the admin

        $messageData = [
            'message_id' => $uid,
            'in_reply_to' => $inReplyTo,
            'folder' => 'sent',
            'to' => implode(',', $to),
            'from' => $sender['email'],
            'from_name' => $sender['name'],
            'subject' => $subject,
            'body_html' => $body,
            'recipients' => 0,
            'sender' => $sender['id'],
            'total_recipients' => 1,
        ];

        return $messageData;
    }

    public function sendMessageToAdmin($sender = 0, $post = [])
    {
        if (empty($post)) {
            return [
                'success' => false,
                'message' => 'Unable to compose message, missing information.'
            ];
        }

        $messageData = $this->createMessageFromAgent($sender, $post);

        if (isset($post['uid']) && $post['uid'] != '') {
            $message = MessagesModel::where('message_id', $post['uid'])->first();
        } else {
            $message = new MessagesModel;
        }

        foreach ($messageData as $k => $v) {
            $message->{$k} = $v;
        }

        $message->save();

        if ($message->save()) {
            //queue messages to be send individually
            $resp = [
                'success' => true,
                'message' => sprintf('The message has been sent to %s staff.', getenv('APP_NAME'), $message->total_recipients)
            ];
        }

        return $resp;
    }
}
